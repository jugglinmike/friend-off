# Friend Off!

A real-time web application for groups of friends to see how well they know
each other.

## Local development

This project requires [Node.js](https://nodejs.org/en/) version 16. To run it
locally for development purposes, issue the following commands from a terminal
located in the repository's root directory:

    npm install
    npm run dev

## Deploying to production

Deploying this project to production requires [Git](http://git-scm.com/) and
Git "push" access to the remote repository. To deploy, run the following
command:

    npm run deploy

## License

Copyright 2021-2022 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)

[Menu icons provided by Font Awesome under the Creative Commons Attribution 4.0
International license.](https://fontawesome.com/license)

[Internationalization ("i18n")
icon](https://commons.wikimedia.org/wiki/File:%C3%86toms_-_Translation.svg)
provided by [Ætoms](https://commons.wikimedia.org/wiki/User:%C3%86toms) under
the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons
