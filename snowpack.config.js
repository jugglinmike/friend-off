/** @type {import("snowpack").SnowpackUserConfig} */
const config = {
  mount: {
    'src/client': '/',
  },
  exclude: [
    '**/node_modules/**/*',
    'build/**',
  ],
  devOptions: {
    open: 'none',
    output: 'stream',
    hmrPort: 8081,
  },
  plugins: [
    '@snowpack/plugin-typescript',
    'snowpack-plugin-yaml',
    './scripts/snowpack-plugin-licenses.cjs',
    './scripts/snowpack-plugin-source-code-references.cjs',
  ],
  optimize: {
    bundle: true,
    minify: true,
    target: 'es2017',
  },
};

export default config;
