declare namespace Intl {
  class ListFormat {
    constructor(locales?: string | string[], options?: object);
    public format: (items: string[]) => string;
  }
}
