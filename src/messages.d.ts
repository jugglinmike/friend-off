import {Guess, Phase, Player, PlayerName} from './types';

interface PlayerStanding {
  correct: number,
  skipped: number,
  incorrect: number,
}

type PlayerStandings = Record<PlayerName, PlayerStanding>;

type Standings = Record<PlayerName, PlayerStandings>;

interface PlayersMessage {
  name: 'players',
  data: Player[],
}

interface PhaseMessage {
  name: 'phase',
  data: Phase,
}

interface StandingsMessage {
  name: 'standings',
  data: Standings,
}

interface GuessMessage {
  name: 'guess',
  data: {
	guess: Guess,
  },
}

interface JoinMessage {
  name: 'join',
  data: Player,
}

interface QuestionMessage {
  name: 'question',
  data: string,
}

interface AnswerMessage {
  name: 'answer',
  data: string,
}

interface AdvanceMessage {
  name: 'advance',
  data: null,
}

export type Message =
  | PlayersMessage
  | PhaseMessage
  | StandingsMessage
  | GuessMessage
  | JoinMessage
  | QuestionMessage
  | AnswerMessage
  | AdvanceMessage;
