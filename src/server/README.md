# The `src/server` directory

This directory contains the code which powers Friend Off's server component. It
is JavaScript designed to be run in [Node.js](https://nodejs.org/).

The `index-common.js` file is the main entry-point, but that file is not run
directly. Instead, `index-development.js` and `index-production.js` are
available to run the server in local development and production contexts,
respectively. This structure minimizes the risk of accidentally deploying the
development code to the production environment.
