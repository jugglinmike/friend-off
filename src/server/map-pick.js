/**
 * Select a random value from a given Map. Return `null` if the Map is empty.
 * The implementation minimizes memory usage for large Maps.
 *
 * @template {any} K
 * @template {any} V
 * @param {Map<K, V>} map
 *
 * @returns {V|null}
 */
export default (map) => {
	const chosenIndex = Math.floor(Math.random() * map.size);
	let count = 0;

	for (const value of map.values()) {
		if (count === chosenIndex) {
			return value;
		}

		count += 1;
	}

	return null;
};
