/**
 * @typedef { import("../messages").Message } Message
 * @typedef { import("../types").Answer } Answer
 * @typedef { import("../types").Equivalence } Equivalence
 * @typedef { import("../types").Guess } Guess
 * @typedef { import("../types").GuessOutcome } GuessOutcome
 * @typedef { import("../types").Phase } Phase
 * @typedef { import("../types").Player } Player
 * @typedef { import("../types").PlayerName } PlayerName
 * @typedef { import("../types").Question } Question
 * @typedef { import("../types").AnsweringPhase } AnsweringPhase
 * @typedef { import("../types").GuessingPhase } GuessingPhase
 * @typedef { import("../types").ErrorData } ErrorData
 */

/**
 * @typedef {object} RoomState
 * @property {Player[]} players
 * @property {Phase} phase
 */

const ANSWERING_PHASE_DURATION = 60 * 1000;

const MAJORITY_VOTE_PAUSE_DURATION = 10 * 1000;

/**
 * Determine if a given player has participated in the game.
 *
 * @param {Player} player
 * @returns {boolean}
 */
const hasParticipated = (player) => Object.keys(player.standings).length > 0;

/**
 * @param {Player["standings"]} standings
 * @param {string} name
 * @param {GuessOutcome} guessOutcome
 *
 * @returns {void}
 */
const updateStandings = (standings, name, guessOutcome) => {
	if (!(name in standings)) {
		standings[name] = [0, 0, 0];
	}

	if (guessOutcome === 'correct') {
		standings[name][0] += 1;
		return;
	} else if (guessOutcome === 'misattributed') {
		standings[name][1] += 1;
		return;
	} else if (guessOutcome === 'missed') {
		standings[name][2] += 1;
		return;
	}

	// This `return` statement is unreachable. It's included here so that the
	// type checker flags any change that makes the conditional statements
	// above non-exhaustive (e.g. the introduction of a new value for the
	// `GuessOutcome` union type).
	return guessOutcome;
};

/**
 * @param {{[key: string]: Answer}} answers
 * @param {string} author
 * @param {string} otherPlayer
 *
 * @returns {null|Equivalence}
 */
const getEquivalence = (answers, author, otherPlayer) => {
	if (!(author in answers)) {
		return null;
	}

	return answers[author].equivalences
		.find(({author}) => author === otherPlayer) || null;
};

export default class Room {
	/**
	 * Create an instance of a Room to keep track of a game, including all
	 * players (active and inactive) and their scores.
	 *
	 * @param {PlayerName} name
	 * @param {Function} handleMessage
	 * @param {Function} handleEnd
	 */
	constructor(name, handleMessage, handleEnd) {
		this.name = name;
		this._handleMessage = handleMessage;
		this._handleEnd = handleEnd;
		/** @type {null|ReturnType<setTimeout>} */
		this._timer = null;

		/** @type {Map<PlayerName, Player>} */
		this.players = new Map();
		/** @type {Phase} */
		this._phase = {
			name: 'joining',
			round: 0,
			data: {
				done: [],
			},
		};
	}

	/**
	 * Determine if the room is empty--that is, if one or more players is
	 * currently connected to the server.
	 */
	get isEmpty() {
		for (const player of this.players.values()) {
			if (player.cxnId) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Given a connection identifier, return the Player instance which is
	 * currently connected using that identifier (if any such player exists).
	 *
	 * @param {string} cxnId
	 * @returns {null|Player}
	 */
	getPlayerByCxnId(cxnId) {
		for (const candidate of this.players.values()) {
			if (candidate.cxnId === cxnId) {
				return candidate;
			}
		}

		return null;
	}

	/**
	 * Assign a name to a connection. After departing (for instance, due to a
	 * temporary connectivity issue), a player with the same name may re-join
	 * the room via this method, though their connection identifier will
	 * necessarily be different.
	 *
	 * @param {PlayerName} name
	 * @param {string} cxnId
	 *
	 * @returns {ErrorData|null}
	 */
	join(name, cxnId) {
		const player = this.players.get(name);

		if (player && player.cxnId) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		this.players.set(name, {
			name: name,
			avatar: null,
			question: null,
			standings: {},
			...player,
			cxnId,
		});

		this.send({
			name: 'players',
			data: [...this.players.values()],
		});

		return null;
	}

	/**
	 * Store a player's answer to the current question, overwriting any
	 * previously-submitted answer. If no text is provided, only remove the
	 * previously-submitted answer.
	 *
	 * @param {string} cxnId
	 * @param {string|null} text
	 *
	 * @return {ErrorData|null}
	 */
	answer(cxnId, text) {
		if (this.phase.name !== 'answering') {
			return {
				id: 'ERR_ANSWER_OUT_OF_PHASE',
				context: this.phase.name,
			};
		}

		const player = this.getPlayerByCxnId(cxnId);
		const {answers} = this.phase.data;

		if (!player) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		if (!text) {
			delete answers[player.name];
		} else {
			answers[player.name] = {
				author: player.name,
				equivalences: [],
				text
			};
		}

		const activePlayers = [...this.players.values()]
			.filter(({cxnId}) => !!cxnId);
		if (activePlayers.every(({name}) => name in answers)) {
			_completeAnswering(this, 0);
		}

		return null;
	}

	/**
	 * Register a player's desire to advance to the next phase within a round.
	 *
	 * @param {string} cxnId
	 *
	 * @returns {ErrorData|null}
	 */
	advance(cxnId) {
		if (this.phase.name === 'asking' || this.phase.name === 'answering') {
			return {
				id: 'ERR_ADVANCE_OUT_OF_PHASE',
				context: this.phase.name,
			};
		}

		const player = this.getPlayerByCxnId(cxnId);

		if (!player) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		if (this.phase.data.done.includes(player.name)) {
			return {id: 'ERR_PLAYER_ALREADY_ADVANCED'};
		}

		const round = this.phase.round;
		const done = this.phase.data.done.concat(player.name);
		const activePlayers = [...this.players.values()]
			.filter(({cxnId}) => !!cxnId);
		const activeDonePlayers = activePlayers
			.filter(({name}) => done.includes(name));

		if (this.phase.name === 'joining') {
			const shouldTransition = activePlayers.length > 2 &&
				activeDonePlayers.length === activePlayers.length;

			if (shouldTransition) {
				const question = this.chooseQuestion();

				if (!question) {
					this.phase = /** @type Phase */({
						round,
						name: 'asking',
						data: null,
					});
				} else {
					this.phase = /** @type Phase */({
						name: 'answering',
						round,
						data: {
							endTime: Date.now() + ANSWERING_PHASE_DURATION,
							question,
							answers: {},
						},
					});
				}
			} else {
				this.phase = /** @type Phase */({
					name: 'joining',
					round,
					data: {done},
				});
			}

			return null;
		}

		const phaseData = {
			...this.phase.data,
			done,
		};

		if (activeDonePlayers.length === activePlayers.length) {
			// All active players are done, so transition to the next phase
			// immediately by setting the "end time" to one millisecond from
			// now.
			phaseData.endTime = Date.now() + 1;
		} else if (!phaseData.endTime && Math.round(activeDonePlayers.length / activePlayers.length) >= 0.5) {
			// Most active players are done, so begin a countdown to transition
			// to the next phase.
			phaseData.endTime = Date.now() + MAJORITY_VOTE_PAUSE_DURATION;
		}

		this.phase = /** @type {Phase} */({
			name: this.phase.name,
			round: this.phase.round,
			data: phaseData,
		});

		return null;
	}

	/**
	 * Store a player's question (overwriting any question that they previously
	 * submitted) to be considered when choosing an answer for the next round.
	 *
	 * @param {string} cxnId
	 * @param {string} text
	 *
	 * @returns {ErrorData|null}
	 */
	ask(cxnId, text) {
		const player = this.getPlayerByCxnId(cxnId);

		if (!player) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		const timeStamp = Date.now();

		player.question = {text, timeStamp};

		if (this.phase.name === 'asking') {
			this.phase = /** @type Phase */({
				name: 'answering',
				round: this.phase.round,
				data: {
					endTime: Date.now() + ANSWERING_PHASE_DURATION,
					question: {
						author: player.name,
						text,
						timeStamp,
					},
					answers: {},
				},
			});
		}

		return null;
	}

	/**
	 * Store a player's suspicion about the author of a given answer (as
	 * determined by the name of the answer's true author), replacing any
	 * previously-submitted guess. If no suspected author is provided, only
	 * remove the previously-submitted guess.
	 *
	 * @param {string} cxnId
	 * @param {string} actualAuthor
	 * @param {string|null} suspectedAuthor
	 *
	 * @returns {ErrorData|null}
	 */
	guess(cxnId, actualAuthor, suspectedAuthor) {
		if (this.phase.name !== 'guessing') {
			return {
				id: 'ERR_GUESS_OUT_OF_PHASE',
				context: this.phase.name,
			};
		}

		const player = this.getPlayerByCxnId(cxnId);

		if (!player) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		if (!this.phase.data.guesses[player.name]) {
			this.phase.data.guesses[player.name] = {};
		}

		if (!suspectedAuthor) {
			delete this.phase.data.guesses[player.name][actualAuthor];

			if (!Object.keys(this.phase.data.guesses[player.name]).length) {
				delete this.phase.data.guesses[player.name];
			}
		} else {
			this.phase.data.guesses[player.name][actualAuthor] = {
				actualAuthor,
				suspectedAuthor,
			};
		}

		return null;
	}

	/**
	 * Mark a player as inactive or remove them completely if they have not yet
	 * participated. If this departure changes the status of a phase-transition
	 * vote (i.e. by causing the number of "votes to advance" to reflect a
	 * majority), then trigger a transition to the next phase.
	 *
	 * @param {string} cxnId
	 */
	leave(cxnId) {
		const player = this.getPlayerByCxnId(cxnId);

		if (!player) {
			return;
		}

		// If the departing player has participated, then nullify their
		// connection ID, signaling their departure to clients while allowing
		// them to render information about their performance. If the departing
		// player has *not* participated, remove them from the room entirely so
		// that clients no longer display them in any form.
		if (hasParticipated(player)) {
			player.cxnId = null;
		} else {
			this.players.delete(player.name);
		}

		if (this.isEmpty) {
			this._handleEnd();
			return;
		}

		this.send({
			name: 'players',
			data: [...this.players.values()],
		});

		if (this.phase.name === 'answering') {

			const {answers} = this.phase.data;
			const activePlayers = [...this.players.values()]
				.filter(({cxnId}) => !!cxnId);
			if (activePlayers.every(({name}) => name in answers)) {
				_completeAnswering(this, 0);
			}
		} else if (this.phase.name === 'guessing' && !this.phase.data.endTime) {
			const done = this.phase.data.done;
			const activePlayers = [...this.players.values()]
				.filter(({cxnId}) => !!cxnId);
			const activeDonePlayers = activePlayers
				.filter(({name}) => done.includes(name));

			if (Math.round(activeDonePlayers.length / activePlayers.length) >= 0.5) {
				this.phase.data.endTime = Date.now() + MAJORITY_VOTE_PAUSE_DURATION;
				this.send({name: 'phase', data: this.phase});
			}
		}
	}


	/**
	 * Indicate that two answers are equivalent. Only the author of an answer
	 * can make a claim about its equivalence to another. This claim is only
	 * verified if the author of the corresponding answer makes a similar
	 * claim.
	 *
	 * @param {string} cxnId
	 * @param {string|null} otherPlayerName
	 *
	 * @return {ErrorData|null}
	 */
	match(cxnId, otherPlayerName) {
		if (this.phase.name !== 'guessing') {
			return {
				id: 'ERR_MATCH_OUT_OF_PHASE',
				context: this.phase.name,
			};
		}

		const player = this.getPlayerByCxnId(cxnId);
		const {answers} = this.phase.data;

		if (!player) {
			return {id: 'ERR_UNRECOGNIZED_CXN'};
		}

		if (!otherPlayerName || !(otherPlayerName in answers)) {
			return {id: 'ERR_ANSWER_NOT_FOUND', context: otherPlayerName};
		}

		const answer = answers[player.name];

		if (!answer) {
			return {id: 'ERR_ANSWER_NOT_FOUND', context: player.name};
		}

		if (player.name === otherPlayerName) {
			return null;
		}

		const otherEquivalence = getEquivalence(
			answers, otherPlayerName, player.name
		);

		if (!getEquivalence(answers, player.name, otherPlayerName)) {
			answer.equivalences.push({
				author: otherPlayerName,
				verified: !!otherEquivalence,
			});
		}

		if (otherEquivalence && !otherEquivalence.verified) {
			otherEquivalence.verified = true;

			// Refresh all clients' state so that players learn of the
			// newly-verified equivalence.
			this.send({name: 'phase', data: this.phase});
		}

		return null;
	}

	get phase() {
		return this._phase;
	}

	/**
	 * @param {Phase} value
	 */
	set phase(value) {
		this._phase = value;
		this.send({name: 'phase', data: value});

		if (value.name === 'answering') {
			const {endTime, question} = value.data;

			const author = this.players.get(question.author);
			if (author) {
				author.question = null;
			}

			// Update the player listing on clients so they respond to the
			// newly-deleted "draft" Question.
			this.send({
				name: 'players',
				data: [...this.players.values()],
			});

			_completeAnswering(this, endTime);
		} else if (value.name === 'guessing' && value.data.endTime) {
			_completeGuessing(this, value.data.endTime);
		} else if (value.name === 'reviewing' && value.data.endTime) {
			_completeReviewing(this, value.data.endTime);
		}
	}

	// BEGIN NOTABLE SOURCE CODE REGION "CHOOSE_QUESTION"
	/**
	 * Select a question for the next "answering" phase, preferring the
	 * question which has been in the queue for the longest amount of time.
	 *
	 * @returns {Question|null}
	 */
	chooseQuestion() {
		const player = [...this.players.values()]
			.reduce((prev, next) => {
				if (!prev || !prev.question) {
					return next.question ? next : null;
				}
				if (!next.question) {
					return prev;
				}
				return prev.question.timeStamp < next.question.timeStamp ?
					prev : next;
			}, /**@type {null|Player}*/(null));

		if (!player) {
			return null;
		}

		return {
			author: player.name,
			text: '',
			timeStamp: 0,
			...player.question,
		};
	}
	// END NOTABLE SOURCE CODE REGION "CHOOSE_QUESTION"

	/**
	 * @param {Message} message
	 */
	send(message) {
		this._handleMessage(message);
	}

	/**
	 * @returns {RoomState}
	 */
	state() {
		return {
			players: [...this.players.values()],
			phase: this.phase,
		};
	}

}

/**
 * Create a delayed "phase transition" function from a provided
 * fully-synchronous version, taking care to ensure that no more than one
 * transition is scheduled at any moment.
 *
 * @param {(room: Room) => void} fn
 *
 * @returns {(room: Room, endTime: number) => void}
 */
const wrapTransitionFn = (fn) => {
	return (room, endTime) => {
		if (room._timer) {
			clearTimeout(room._timer);
		}

		room._timer = setTimeout(() => {
			room._timer = null;

			fn(room);
		}, endTime - Date.now());
	};
};

const _completeAnswering = wrapTransitionFn((room) => {
	const phase= /**@type {AnsweringPhase}*/(room.phase);
	const answers = JSON.parse(JSON.stringify(
		phase.data.answers
	));
	room.phase = /** @type Phase */({
		name: 'guessing',
		round: phase.round,
		data: {
			endTime: null,
			question: phase.data.question,
			done: [],
			answers,
			guesses: {},
		},
	});
});

const _completeGuessing = wrapTransitionFn((room) => {
	if (room._phase.name !== 'guessing') {
		return;
	}

	const {guesses} = /**@type {GuessingPhase}*/(room.phase).data;
	for (const [guesserName, playerGuesses] of Object.entries(guesses)) {
		const guesser = room.players.get(guesserName);
		if (!guesser) {
			continue;
		}

		const standings = {...guesser.standings};
		for (const guess of Object.values(playerGuesses)) {
			const {actualAuthor, suspectedAuthor} = guess;
			if (suspectedAuthor === null) {
				continue;
			}

			// BEGIN NOTABLE SOURCE CODE REGION "SCORE_ANSWER"
			if (actualAuthor === suspectedAuthor) {
				updateStandings(standings, actualAuthor, 'correct');
			} else {
				const {answers} = room._phase.data;
				const equivalence = getEquivalence(
					answers, actualAuthor, suspectedAuthor
				);

				if (equivalence && equivalence.verified) {
					updateStandings(standings, actualAuthor, 'correct');
				} else {
					updateStandings(standings, suspectedAuthor, 'misattributed');
					updateStandings(standings, actualAuthor, 'missed');
				}
			}
			// END NOTABLE SOURCE CODE REGION "SCORE_ANSWER"
		}

		room.players.set(guesserName, {
			...guesser,
			standings,
		});
	}

	room.send({
		name: 'players',
		data: [...room.players.values()],
	});
	room.phase = {
		name: 'reviewing',
		round: room._phase.round,
		data: {
			...room._phase.data,
			endTime: null,
			done: [],
		},
	};
});

const _completeReviewing = wrapTransitionFn((room) => {
	if (room._phase.name !== 'reviewing') {
		return;
	}

	const question = room.chooseQuestion();
	const round = room.phase.round + 1;

	if (!question) {
		room.phase = {
			name: 'asking',
			round,
			data: null,
		};
	} else {
		room.phase = {
			name: 'answering',
			round,
			data: {
				endTime: Date.now() + ANSWERING_PHASE_DURATION,
				question,
				answers: {},
			},
		};
	}
});
