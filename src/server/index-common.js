import express from 'express';
import * as http from 'http';
import {Server as SocketIo} from 'socket.io';
import debug_ from 'debug';

import * as glitchWorkaround from './glitch-workaround.js';
import mapPick from './map-pick.js';
import Room from './room.js';
import {
  validateGuess, validateNull, validatePlayerName, validateRoomName,
  validateText,
} from '../client/validation.js';

const app = express();
const server = new http.Server(app);
const io = new SocketIo(server, {...glitchWorkaround.socketIoOptions});
const debug = debug_('index');
// Number of milliseconds to wait between sending intermittent instructions for
// a keepalive signal.
const KEEPALIVE_PERIOD = 30 * 1000;

/**
 * @typedef { import("socket.io-client").Socket } Socket
 * @typedef {import("../messages").Message} Message
 * @typedef {import("../types").Command} Command
 * @typedef { import("../types").ErrorData } ErrorData
 */

/**
 * @template T
 * @typedef {import("../types").Validator<T>} Validator<T>
 */

/**
 * @type {Map<string, Room>}
 */
const rooms = new Map();

/**
 * Broadcast a message to all members of a given room. Defining the integration
 * with Socket.io in a dedicated function allows the `Room` abstraction to be
 * agnostic of that library.
 *
 * @param {string} roomName
 * @param {Message} message
 */
const genericHandleMessage = (roomName, message) => {
	io.to(roomName).emit(message.name, message.data);
};

// BEGIN NOTABLE SOURCE CODE REGION "DESTROY_EMPTY_ROOM"
/**
 * @param {string} roomName
 */
const genericHandleEnd = (roomName) => {
	rooms.delete(roomName);
};
// END NOTABLE SOURCE CODE REGION "DESTROY_EMPTY_ROOM"

/**
 * @template T
 * @param {Validator<T>} validate
 * @param {(input: T) => null | ErrorData} handler
 */
const wrapCommandHandler = (validate, handler) => {
	/**
	 * @param {Command} command
	 *
	 * @this {Socket}
	 */
	function wrapped(command) {
		const result = validate(command.value);

		if (!result.isValid) {
			const error = {
				id: 'ERR_INVALID_INPUT',
				context: result.reason,
			};
			debug(error);
			this.emit('result', {id: command.id, error});
			return;
		}

		/** @type {ErrorData|null} */
		let error = null;

		try {
			error = handler(result.value);
		} catch (caught) {
			error = {
				id: 'ERR_UNKNOWN',
				context: caught.message,
			};
		}

		this.emit('result', {id: command.id, error});
	}

	return wrapped;
};

io.use((socket, next) => {
	const result = validateRoomName(String(socket.handshake.auth.roomName));

	if (!result.isValid) {
		next(new Error(result.reason));
		return;
	}
	const roomName = result.value;

	if (!rooms.has(roomName)) {
		const handleMessage = genericHandleMessage.bind(null, roomName);
		const handleEnd = genericHandleEnd.bind(null, roomName);
		const newRoom = new Room(roomName, handleMessage, handleEnd);
		rooms.set(roomName, newRoom);
	}
	const room = /**@type Room*/(rooms.get(roomName));

	socket.join(roomName);

	socket.on('join', wrapCommandHandler(validatePlayerName, (name) => {
		return room.join(name, socket.id);
	}));

	socket.on('ask', wrapCommandHandler(validateText, (text) => {
		const error = room.ask(socket.id, text);
		if (error) {
			return error;
		}

		socket.emit('players', [...room.players.values()]);

		return null;
	}));

	socket.on('answer', wrapCommandHandler(validateText, (text) => {
		const error = room.answer(socket.id, text);

		if (error) {
			return error;
		}

		socket.emit('state', room.state());

		return null;
	}));

	socket.on('guess', wrapCommandHandler(validateGuess, (guess) => {
		const {actualAuthor, suspectedAuthor} = guess;
		return room.guess(socket.id, actualAuthor, suspectedAuthor);
	}));

	socket.on('match', wrapCommandHandler(validateText, (playerName) => {
		const error = room.match(socket.id, playerName);

		if (error) {
			return error;
		}

		socket.emit('state', room.state());

		return null;
	}));

	socket.on('advance', wrapCommandHandler(validateNull, () => {
		return room.advance(socket.id);
	}));

	socket.on('disconnect', () => {
		room.leave(socket.id);
	});

	socket.emit('state', room.state());

	next();
});

app.get('/.status', (req, res) => {
	let players = 0;
	for (const room of rooms.values()) {
		players += room.players.size;
	}
	res.json({rooms: rooms.size, players});
});

app.get(/^\/[^.\/]+$/, glitchWorkaround.expressMiddleware, (req, res, next) => {
	req.url = '/index.html';
	next();
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
	console.log(`listening on port ${PORT}`);
});

/**
 * This periodic task is an affordance for the particulars of the Glitch
 * platform, which automatically shuts down servers that have not received
 * HTTP(S) traffic after some duration.
 *
 * Select a random client and instruct it to send an HTTP back to the server.
 * This traffic ensures the server remains active as long as at least one
 * WebSocket client is present.
 *
 * See the `useKeepalive` hook in the `client` directory for more information.
 */
setInterval(() => {
	const socket = mapPick(io.sockets.sockets);
	if (!socket) {
		return;
	}

	socket.emit('keepalive');
}, KEEPALIVE_PERIOD);

export default app;
