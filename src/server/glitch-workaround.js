// This module defines an Express.js middleware function designed to circumvent
// the following limitation of the Glitch platform (a web application hosting
// service where the original implementation of this project is deployed):
//
// > # How do I get WebSocket to connect with my projects that use custom domains?
// >
// > Currently, WebSocket connections do not work with custom domains on Glitch.
// >
// > If your Glitch projects use custom domains and WebSocket, you will need to
// > set them up so that the projects are connected via their glitch.me
// > domains.
//
// Source: https://glitch.happyfox.com/kb/article/129-how-do-i-get-websocket-to-connect-with-my-projects-that-use-custom-domains/
//
// By defining the Glitch-assigned domain as a cookie, the front-end execution
// context can connect via that domain.

/**
 * @typedef { import("express").Response } Response
 * @typedef { import("express").NextFunction } NextFunction
 */

const {PROJECT_DOMAIN} = process.env;
const websocketDomain = PROJECT_DOMAIN ?
	`${PROJECT_DOMAIN}.glitch.me` : null;

export const socketIoOptions = PROJECT_DOMAIN ?
	{
		cors: {
			origin: [
				'https://friendoff.mikepennisi.com',
				'https://friendoff.net',
				'http://friendoff.net'
			],
			methods: ['GET', 'POST']
		}
	} : {};

export const expressMiddleware = PROJECT_DOMAIN ?
	/**
	 * @param {object} req
	 * @param {Response} res
	 * @param {NextFunction} next
	 */
	(req, res, next) => {
		res.append(
			'Set-Cookie', `websocket-domain=${websocketDomain}; SameSite=Lax`
		);
		next();
	} :
	/**
	 * @param {object} req
	 * @param {Response} res
	 * @param {NextFunction} next
	 */
	(req, res, next) => next();
