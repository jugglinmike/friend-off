import {startServer, createConfiguration} from 'snowpack';
import debug_ from 'debug';

import app from './index-common.js';
import config from '../../snowpack.config.js';

const serverPromise = startServer({
	lockfile: null,
	config: createConfiguration(config),
});
const debug = debug_('index');

app.use(async (req, res, next) => {
	const server = await serverPromise;
	try {
		const buildResult = await server.loadUrl(req.url);

		// Snowpack does not document the conditions which would cause its
		// `loadUrl` function to return `undefined`, but since the type
		// declarations include that value, it must be handled, somehow.
		if (!buildResult) {
			debug('empty buildResult');
			next();
			return;
		}

		const contentType = buildResult.contentType || 'text/plan';
		res.writeHead(200, {'Content-Type': contentType});
		res.write(buildResult.contents);
		res.end();
	} catch (err) {
		debug('error', req.url, err);
		next();
	}
});
