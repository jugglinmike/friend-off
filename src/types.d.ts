export type PlayerName = string;

export type Player = {
	name: PlayerName;
	avatar: null | string;
	question: null | Omit<Question, 'author'>;
	cxnId: null | string;
	standings: {
		[key: string]: [
			number, // total # of correct guesses for person named by key
			number, // total # of guesses misattributed to person named by key
			number, // total # of guesses missed (not just skipped) from person named by key
		];
	};
};

export type GuessOutcome = 'correct' | 'misattributed' | 'missed';

export interface Question {
	author: PlayerName;
	text: string;
	timeStamp: number;
}

export interface Equivalence {
	author: PlayerName;
	verified: boolean;
}

export interface Answer {
	author: PlayerName;
	/**
	 * The names of players whose answers this author has indicated are
	 * equivalent to their own.
	 */
	equivalences: Equivalence[];
	text: string;
}

export interface Guess {
	actualAuthor: PlayerName;
	suspectedAuthor: PlayerName | null;
}

export interface AllGuesses {
	[key: string]: {
		/**
		 * Keyed on the "actual" author so that players can guess that
		 * multiple answers may have come from one person (as in, "Joe
		 * definitely said one of these two answers.")
		 */
		[key: string]: Guess,
	},
}

export interface AllAnswers {
	[key: string]: Answer,
}

export interface JoiningPhase {
	name: 'joining';
	round: number;
	data: {
		done: PlayerName[];
	};
}

export interface AskingPhase {
	name: 'asking';
	round: number;
	data: null;
}

export interface AnsweringPhase {
	name: 'answering';
	round: number;
	data: {
		endTime: number,
		question: Question,
		answers: AllAnswers,
	};
}

export interface GuessingPhase {
	name: 'guessing';
	round: number;
	data: {
		endTime: number | null;
		question: Question;
		done: PlayerName[];
		answers: AllAnswers;
		guesses: AllGuesses;
	};
}

export interface ReviewingPhase {
	name: 'reviewing';
	round: number;
	data: {
		endTime: number | null;
		question: Question;
		done: PlayerName[];
		answers: AllAnswers;
		guesses: AllGuesses;
	};
}

export type Phase =
	| JoiningPhase
	| AskingPhase
	| AnsweringPhase
	| GuessingPhase
	| ReviewingPhase;

export type Validation<Cleaned> =
	| { isValid: true, value: Cleaned }
	| { isValid: false, reason: string };

export type Validator<Cleaned> = (input: any) => Validation<Cleaned>;

export type ErrorID =
	| 'ERR_ADVANCE_OUT_OF_PHASE'
	| 'ERR_ANSWER_NOT_FOUND'
	| 'ERR_ANSWER_OUT_OF_PHASE'
	| 'ERR_GUESS_OUT_OF_PHASE'
	| 'ERR_MATCH_OUT_OF_PHASE'
	| 'ERR_LOST_CXN'
	| 'ERR_PLAYER_ALREADY_ADVANCED'
	| 'ERR_UNKNOWN'
	| 'ERR_UNRECOGNIZED_CXN';

export interface ErrorData {
	id: ErrorID,
	context?: any,
}

export interface Command {
	id: number,
	value: string | null,
}

export interface CommandResult {
	id: number;
	error: ErrorData | null;
}

export interface ErrorLogEntry {
	timestamp: Date;
	data: ErrorData;
}
