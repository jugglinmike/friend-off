declare module '*.licenses' {
	const licenses: Array<{
		name: string;
		version: string;
		repository: string;
		licenseName: string;
		licenseFile: string;
		license: {
			name: string;
			text: string;
		};
	}>;

	export default licenses;
}
