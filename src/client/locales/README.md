# The `src/cient/locales` directory

<!-- BEGIN NOTABLE SOURCE CODE REGION "HOW_TO_TRANSLATE" -->
This directory stores all the snippets of text that a player might read while
playing Friend Off. The set of statements, phrases, and words are available in
every spoken language that Friend Off supports. Each
[YAML](https://yaml.org/)-formatted file includes the text for one language.

If you would like to translate Friend Off into a language which isn't yet
supported, this is the place to start! Copy the file named `en-us.yaml` into a
new file name with [the IETF language
tag](https://en.wikipedia.org/wiki/IETF_language_tag) of your desired language,
and translate all the "values" in the YAML "dictionary," leaving the "keys"
intact.

For example, imagine you wanted to translate Friend Off into Portugal
Portuguese. In that case, and if the `en-us.yml` file looked like this:

```yaml
---
HELLO: Hello
GOODNIGHT: Goodnight
```

You would create a file named `pt-pt.yml` with the following contents:

```yaml
---
HELLO: Olá
GOODNIGHT: Boa noite
```

If you're at all uncertain, don't hesitate to ask questions or share your
work-in-progress!
<!-- END NOTABLE SOURCE CODE REGION "HOW_TO_TRANSLATE" -->
