export default {
	enter: 13,
	escape: 27,
	space: 32,
	end: 35,
	home: 36,
	up: 38,
	down: 40
};
