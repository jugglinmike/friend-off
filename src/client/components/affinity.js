import html from '../html';
import {useLocalize} from '../locale';

import AffinityGraph from './affinity-graph';
import AffinityTable from './affinity-table';
import ExternalLink from './external-link';
import {SCORE_ANSWER} from '../dummy.notable-source-code-regions';

/** @typedef { import("../player").default } Player */

/**
 * @param {object} props
 * @param {Player[]} props.players
 */
export default ({players}) => {
	const localize = useLocalize();
	return html`
		<p>
			${localize('AFFINITY_CHART_DESC')}
			<${ExternalLink} href="${SCORE_ANSWER}">
				${localize('AFFINITY_CHART_SOURCE_LINK_TEXT')}
			</${ExternalLink}>
		</p>
		<${AffinityGraph} players=${players} />

		<p>${localize('AFFINITY_TABLE_DESC')}</p>
		<${AffinityTable} players=${players} />
	`;
};
