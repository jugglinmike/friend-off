import {useLocalize} from '../locale';
import html from '../html';

export default () => {
	const localize = useLocalize();

	return html`
		<p>${localize('PHASE_JOINING_DESC')}</p>

		<p>${localize('PHASE_JOINING_SUGGESTION')}</p>
	`;
};
