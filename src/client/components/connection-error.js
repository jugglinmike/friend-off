import html from '../html';
import {useLocalize} from '../locale';

export default () => {
	const localize = useLocalize();
	return html`
		<p>${localize('CONNECTION_ERROR_DESC')}</p>
	`;
};
