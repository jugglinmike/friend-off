# The `src/client` directory

This directory contains the code which power web browsers which visit the
Friend Off website.

Most of the files are JavaScript files; these make heavy use of [the Preact
framework](https://preactjs.com/) in order to succinctly define the
application's user interface. Friend Off integrates Preact with [the htm
library](https://github.com/developit/htm) in order to avoid the proprietary
JSX programming language.

This directory also include CSS files. These are inserted into the application
according to the ES6 `import` statements. They are not modified or interpreted
beyond this.

The project uses [Snowpack](https://www.snowpack.dev/) to serve these files
directly to the browser in local development contexts. Snowpack uses
[esbuild](https://esbuild.github.io/) to produce an optimized file for use in
production contexts. You can learn more about the project's build process by
reviewing the directory named "scripts".
