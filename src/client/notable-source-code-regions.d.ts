declare module '*.notable-source-code-regions' {
  export const CHOOSE_QUESTION: string;
  export const DESTROY_EMPTY_ROOM: string;
  export const HOW_TO_TRANSLATE: string;
  export const SCORE_ANSWER: string;
  export const WHAT_MAKES_IT_FOSS_UI: string;
}
