import { render } from 'preact';
import html from './html';
import {io} from 'socket.io-client';

import './index.css';
import Landing from './components/landing';
import Room from './components/room';
import {validateRoomName} from './validation';

const result = validateRoomName(location.pathname.slice(1));
const main = document.getElementsByTagName('main')[0];

const websocketDomain = (document.cookie
	.split('; ')
	.find((row) => row.startsWith('websocket-domain=')) || '')
	.split('=')[1];

if (result.isValid) {
	const socket = io(websocketDomain, {auth: {roomName: result.value}});
	render(html`<${Room} socket=${socket} />`, main);
} else {
	render(html`<${Landing} />`, main);
}
