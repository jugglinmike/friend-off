# The `src` directory

This directory contains the code which makes Friend Off run. Most of the files
are placed within one of two sub-directories: "client" (for "client-side" code
intended for execution in web browser) and "server" (for the code which powers
the web server).

This directory also contains some type declarations expressed in
[TypeScript](https://www.typescriptlang.com/). Although the project's
executable code is written in JavaScript and *not* TypeScript, these types are
referenced by JavaScript code comments which are recognized by the TypeScript
compiler and verified as part of the project's automated test suite. You can
learn more about the project's test suite by reviewing the directory named
"test".
