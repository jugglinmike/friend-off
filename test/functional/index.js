/* jshint -W083 */
import assert from 'assert';
import child_process from 'child_process';
import {fileURLToPath} from 'url';
import path from 'path';

import {io} from 'socket.io-client';
import {firstValueFrom, lastValueFrom, Observable} from 'rxjs';
import {filter, map, take, timeout, toArray} from 'rxjs/operators';
import seedrandom from 'seedrandom';

/** @typedef { import("socket.io-client").Socket } Socket */
/** @typedef { import("../../src/types").Player } PlayerData */
/** @typedef {import("./types").PlayerGuesses} PlayerGuesses */
/** @typedef {import("./types").Guesses} Guesses */
/** @typedef {import("./types").Standings} Standings */

const ROUND_COUNT = parseInt(process.argv[2], 10);
const RANDOM_SEED = process.argv[3] || `seed-${Math.random()}`;
const CONNECT_TIMEOUT = 2 * 1000;
const COMMAND_TIMEOUT = 1000;
const ASSERTION_TIMEOUT= 1000;

const dirname = path.dirname(fileURLToPath(import.meta.url)); // jshint ignore:line

if (!Number.isInteger(ROUND_COUNT) || ROUND_COUNT < 1) {
	throw new Error(`Invalid round count: "${ROUND_COUNT}"`);
}

/**
 * @param {(connect: () => Promise<Socket>, roundCount: number, randomSeed: string) => void} body
 */
const test = (body) => {
	const PORT = '1234';

	const child = child_process.spawn('node', ['.'], {
		env: {...process.env, PORT},
		cwd: path.resolve(dirname, '..', '..'),
	});
	let stdError = '';
	child.stderr.on('data', (part) => stdError += part);
	child.stdout.on('data', (p) => console.log(String(p)));
	const onClose = new Promise((_, reject) => {
		child.once('close', () => {
			reject(`Child closed unexpectedly\n${stdError}`);
		});
	});

	/** @type {Socket[]} */
	const sockets = [];
	const connect = async () => {
		await new Promise((resolve) => setTimeout(resolve, 2000));
		const start = Date.now();
		/** @type {Socket} */
		let socket;

		do {
			const _socket = io(
				`http://localhost:${PORT}`,
				{transports: ['websocket'], auth: {roomName: 'test'}}
			);
			socket = _socket;

			try {
				
				await /** @type {Promise<void>} */(new Promise((resolve, reject) => {
					socket.on('connect', resolve);
					socket.once('connect_error', reject);
				}));
				break;
			} catch (e) {}
		} while (Date.now() - start < CONNECT_TIMEOUT);

		if (!socket.connected) {
			throw new Error('Unable to connect');
		}

		sockets.push(socket);

		return socket;
	};

	console.log(`Simulating ${ROUND_COUNT} rounds with random seed "${RANDOM_SEED}"...`);

	return Promise.race([body(connect, ROUND_COUNT, RANDOM_SEED), onClose])
		.then(() => {
			console.log('test completed successfully');
		}, (error) => {
			console.error(error);
			process.exitCode = 1;
		})
		.then(() => {
			sockets.forEach((socket) => {
				socket.disconnect();
			});
			sockets.length = 0;
			child.kill();
		});
};

let commandCount = 0;
/**
 * @param {Player} player
 * @param {string} name
 * @param {any} value
 *
 * @returns {Promise<void>}
 */
const command = async (player, name, value) => {
	const id = ++commandCount;
	player.socket.emit(name, { id, value });

	await firstValueFrom(
		player.observable
			.pipe(filter(({event}) => event === 'result'))
			.pipe(filter(({value}) => value.id === id))
			.pipe(timeout(COMMAND_TIMEOUT))
			.pipe(map(() => null))
	);
};

/**
 * @param {Player[]} players
 * @param {number} count
 * @param {(phases: any[]) => void} validate
 */
const collectPhases = (players, count, validate) => {
	const actual = players.map((player) =>
			player.observable
				.pipe(filter(({event}) => event == 'phase'))
				.pipe(map(({value}) => value))
				.pipe(take(count))
				.pipe(toArray())
				.pipe(timeout(ASSERTION_TIMEOUT))
		)
		.map(lastValueFrom)
		.map((promise) => promise.then(validate));

	return Promise.all(actual);
};

/**
 * @param {Player[]} players
 * @param {Standings} expected
 */
const assertStandings = async (players, expected) => {
	const actual = players.map((player) =>
			player.observable
				.pipe(filter(({event}) => event === 'players'))
				.pipe(timeout(ASSERTION_TIMEOUT))
		)
		.map(firstValueFrom);
	const allActual = (await Promise.all(actual))
		.map(
			/**
			 * @param {object} x
			 * @param {PlayerData[]} x.value
			 */
			({value}) => {
				return value.reduce((all, playerData) => {
					all[playerData.name] = playerData.standings;
					return all;
				}, /** @type Standings */({}));
			}
		);
	const allExpected = players.map(() => expected);
	assert.deepEqual(allActual, allExpected);
};

/**
 * @typedef {object} Player
 * @property {string} name
 * @property {Socket} socket
 * @property {Observable<{event: string, value: any}>} observable
 */

/**
 * @param {() => Promise<Socket>} connect
 *
 * @returns Player
 */
const makePlayer = async (connect) => {
	const socket = await connect();
	const observable = new Observable((subscriber) => {
		/**
		 * @param {string} event
		 * @param {any} value
		 */
		const listener = (event, value) => {
			subscriber.next({event, value});
		};
		socket.onAny(listener);

		return () => {
			socket.offAny(listener);
		};
	});
	return {
		name: '',
		socket,
		observable,
	};
};

test(async (connect, roundCount, randomSeed) => {
	const players = await Promise.all([
		makePlayer(connect), makePlayer(connect), makePlayer(connect),
	]);
	const [player1, player2, player3] = players;
	const rng = seedrandom(randomSeed);

	await Promise.all([
		command(player1, 'join', 'mike'),
		command(player2, 'join', 'mark'),
		command(player3, 'join', 'matt'),
	]);
	player1.name = 'mike';
	player2.name = 'mark';
	player3.name = 'matt';

	await Promise.all([
		command(player1, 'advance', null),
		command(player2, 'advance', null),
		collectPhases(players, 1, ([phase]) => {
			assert.deepEqual([phase.name, phase.round], ['joining', 0]);
		}),
	]);

	await Promise.all([
		command(player3, 'advance', null),
		collectPhases(players, 1, ([phase]) => {
			assert.deepEqual([phase.name, phase.round], ['asking', 0]);
		}),
	]);

	/** @type Standings */
	let expectedStandings = {};
	for (let round = 0; round < roundCount; round += 1) {
		await Promise.all([
			command(player1, 'ask', 'Why?'),
			collectPhases(players, 1, ([phase]) => {
				assert.deepEqual(
					[phase.name, phase.round], ['answering', round]
				);
			}),
		]);

		await Promise.all([
			command(player1, 'answer', 'because'),
			command(player2, 'answer', 'because'),
			command(player3, 'answer', 'because'),
			collectPhases(players, 1, ([phase]) => {
				assert.deepEqual(
					[phase.name, phase.round], ['guessing', round]
				);
			}),
		]);

		const guesses = makeGuesses(players, rng);
		expectedStandings = updateStandings(expectedStandings, guesses);

		const commands = [
			...makeGuessCommands(players, guesses),
			() => Promise.all([
				collectPhases(players, 1, ([phase]) => {
					assert.deepEqual(
						[phase.name, phase.round], ['guessing', round]
					);
				}),
				command(player1, 'advance', null),
			]),
			() => Promise.all([
				collectPhases(players, 1, ([phase]) => {
					assert.deepEqual(
						[phase.name, phase.round], ['guessing', round]
					);
				}),
				command(player2, 'advance', null),
			]),
		].sort(() => rng() > 0.5 ? 1 : -1);

		for (const command of commands) {
			await command();
		}

		await Promise.all([
			collectPhases(players, 2, ([first, second]) => {
				assert.deepEqual(
					[first.name, first.round, second.name, second.round],
					['guessing', round, 'reviewing', round]
				);
				assert.deepEqual(second.data.guesses, guesses);
			}),
			assertStandings(players, expectedStandings),
			command(player3, 'advance', null),
		]);

		await Promise.all([
			collectPhases(players, 1, ([phase]) => {
				assert.deepEqual(
					[phase.name, phase.round], ['reviewing', round]
				);
			}),
			command(player1, 'advance', null),
		]);

		await Promise.all([
			collectPhases(players, 1, ([phase]) => {
				assert.deepEqual(
					[phase.name, phase.round], ['reviewing', round]
				);
			}),
			command(player2, 'advance', null),
		]);

		await Promise.all([
			collectPhases(players, 2, ([first, second]) => {
				assert.deepEqual(
					[first.name, first.round, second.name, second.round],
					['reviewing', round, 'asking', round + 1]
				);
			}),
			command(player3, 'advance', null),
		]);
	}
});

/**
 * @param {Player[]} players
 * @param {() => Number} rng
 *
 * @returns {Guesses}
 */
const makeGuesses = (players, rng) => {
	/** @type Guesses */
	const guesses = {};

	players.forEach((guessingPlayer) => {
		/** @type PlayerGuesses */
		const playerGuesses = {};

		const hasGuesses = players
			.filter((player) => player !== guessingPlayer)
			.some((otherPlayer) => {
				const x = rng();
				if (x < 0.3) {
					return false;
				}
				const actualAuthor = otherPlayer.name;
				let suspectedAuthor;
				
				if (x < 0.6) {
					suspectedAuthor= actualAuthor;
				} else {
					const player = players
						.find((player) => {
							return player !== guessingPlayer && player !== otherPlayer;
						});
					if (!player) {
						throw new Error(`Unable to locate player ${otherPlayer}`);
					}
					suspectedAuthor = player.name;
				}
				playerGuesses[actualAuthor] = {actualAuthor, suspectedAuthor};
				return true;
			});

		if (hasGuesses) {
			guesses[guessingPlayer.name] = playerGuesses;
		}
	});

	return guesses;
};

/**
 * @param {Guesses} guesses
 */
const traverseGuesses = function * (guesses) {
	for (const [guessingPlayer, playerGuesses] of Object.entries(guesses)) {
		for (const [actualAuthor, {suspectedAuthor}] of Object.entries(playerGuesses)) {
			yield {guessingPlayer, actualAuthor, suspectedAuthor};
		}
	}
};

/**
 * @param {Standings} standings
 * @param {Guesses} guesses
 */
const updateStandings = (standings, guesses) => {
	const updated = JSON.parse(JSON.stringify(standings));

	for (const names of traverseGuesses(guesses)) {
		const {guessingPlayer, actualAuthor, suspectedAuthor} = names;

		if (!(guessingPlayer in updated)) {
			updated[guessingPlayer] = {};
		}
		const playerStandings = updated[guessingPlayer];

		if (!(actualAuthor in playerStandings)) {
			playerStandings[actualAuthor] = [0, 0, 0];
		}
		if (!(suspectedAuthor in playerStandings)) {
			playerStandings[suspectedAuthor] = [0, 0, 0];
		}

		if (actualAuthor === suspectedAuthor) {
			playerStandings[actualAuthor][0] += 1;
		} else {
			playerStandings[suspectedAuthor][1] += 1;
			playerStandings[actualAuthor][2] += 1;
		}
	}

	return updated;
};

/**
 * @param {Player[]} players
 * @param {Guesses} guesses
 */
const makeGuessCommands = (players, guesses) => {
	const guessCommands = [];

	for (const names of traverseGuesses(guesses)) {
		const {actualAuthor, suspectedAuthor} = names;
		const guessingPlayer = players.find((player) => {
			return player.name === names.guessingPlayer;
		});
		if (!guessingPlayer) {
			throw new Error(`Unable to find player with name ${names.guessingPlayer}`);
		}

		guessCommands.push(() => {
			return command(guessingPlayer, 'guess', {actualAuthor, suspectedAuthor});
		});
	}

	return guessCommands;
};
