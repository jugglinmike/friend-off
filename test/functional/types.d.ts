export interface PlayerGuesses {
	[key: string]: {
		actualAuthor: string;
		suspectedAuthor: string;
	};
}

export interface Guesses {
	[key: string]: PlayerGuesses;
}

export interface Standings {
	[key: string]: {
		[key: string]: [number, number, number];
	};
}
