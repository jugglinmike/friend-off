import * as assert from 'assert';

import mapPick from '../../src/server/map-pick.js';

suite('mapPick', () => {
	test('chooses a random value from non-empty Map', () => {
		const map = new Map([[0, 23], [1, 45], [2, 99]]);
		/**@type {Array<number|null>}*/
		const candidates = [23 ,45, 99];
		// `mapPick` is non-deterministic by design, so run a bunch of trials.
		let i = 1000;

		while (i--) {
			const picked = mapPick(map);
			assert.notEqual(picked, null, 'Does not return `null`');
			assert.equal(candidates.includes(picked), true, `${picked}`);
		}
	});

	test('returns `null` for an empty Map', () => {
		const map = new Map();
		// `mapPick` is non-deterministic by design, so run a bunch of trials.
		let i = 1000;

		while (i--) {
			const picked = mapPick(map);
			assert.equal(picked, null);
		}
	});
});
