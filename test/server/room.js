import * as assert from 'assert';
import FakeTimers from '@sinonjs/fake-timers';
import Room from '../../src/server/room.js';

/** @typedef {import("../../src/server/room").Message} Message*/
/** @typedef {import("../../src/server/room").Player} Player*/
/** @typedef {import("../../src/types").AnsweringPhase} AnsweringPhase*/
/** @typedef {import("../../src/types").GuessingPhase} GuessingPhase*/
/** @typedef {import("../../src/types").ReviewingPhase} ReviewingPhase*/

const clock = FakeTimers.install();
const ANSWERING_DURATION = 60 * 1000;

/**
 * @param {Message[]} messages
 * @param {string} name
 */
function messageDataByName(messages, name) {
	return messages.filter((message) => message.name === name)
		.map(({data}) => data);
}

suite('room', () => {
	/** @type {Message[]}*/
	const messages = [];
	const ends = [];
	/** @param {Message} message */
	const onMessage = (message) => messages.push(JSON.parse(JSON.stringify(message)));
	const onEnd = () => ends.push(null);
	/** @type {Room} */
	let room;

	setup(() => {
		room = new Room('test-room', onMessage, onEnd);
		messages.length = 0;
		ends.length = 0;
	});

	teardown(() => {
		clock.reset();
	});

	/**
	 * @param {Record<string, string>} players
	 */
	function goToAskingPhase(players) {
		const entries = Object.entries(players);

		for (const [connectionId, name] of entries) {
			room.join(name, connectionId);
			room.advance(connectionId);
		}

		messages.length = 0;
	}

	/**
	 * @param {Record<string, string>} players
	 * @param {object} question
	 * @param {string} question.author
	 * @param {string} question.text
	 * @param {Record<string, string>} answers
	 */
	function goToGuessingPhase(players, question, answers) {
		goToAskingPhase(players);

		room.ask(question.author, question.text);
		for (const [connectionId, text] of Object.entries(answers)) {
			room.answer(connectionId, text);
		}
		clock.tick(ANSWERING_DURATION);

		messages.length = 0;
	}

	suite('answering phase', () => {
		test('transitions to "guessing" phase', () => {
			const startTime = new Date().getTime();
			room.join('mark', 'cxnA');
			room.ask('cxnA', 'Why?');
			room.advance('cxnA');
			clock.tick(1);
			room.join('matt', 'cxnB');
			room.ask('cxnB', 'How?');
			room.advance('cxnB');
			room.join('karen', 'cxnC');
			room.advance('cxnC');
			messages.length = 0;

			room.answer('cxnC', 'Because');
			room.answer('cxnB', 'No');

			clock.tick(ANSWERING_DURATION - 1);

			assert.deepEqual(messages, []);

			clock.tick(1);

			assert.deepEqual(messages, [{
				name: 'phase',
				data: {
					name: 'guessing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'mark',
							text: 'Why?',
							timeStamp: startTime,
						},
						done: [],
						answers: {
							matt: {
								author: 'matt',
								equivalences: [],
								text: 'No',
							},
							karen: {
								author: 'karen',
								equivalences: [],
								text: 'Because',
							},
						},
						guesses: {},
					}
				},
			}]);
		});
	});

	suite('guessing phase', () => {
		test('transitions to "reviewing" phase - no answers and no guesses', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{},
			);
			room.advance('cxnA');
			room.advance('cxnB');
			messages.length = 0;
			const guessingPhase = JSON.parse(JSON.stringify(room.phase));
			clock.tick(10 * 1000 - 1);

			assert.deepEqual(messages, []);
			assert.deepEqual(room.phase, guessingPhase);

			clock.tick(1);

			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'reviewing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {},
						guesses: {},
					},
				}]
			);

			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				]]
			);
		});

		test('transitions to "reviewing" phase - some answers and some guesses', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'Because', cxnB: 'I forget', cxnC: 'Ask me tomorrow'},
			);
			room.guess('cxnA', 'matt', 'karen');
			room.guess('cxnA', 'karen', 'matt');
			room.guess('cxnB', 'karen', 'karen');
			room.guess('cxnC', 'mark', 'mark');
			room.guess('cxnC', 'matt', 'matt');
			room.advance('cxnA');
			room.advance('cxnB');
			messages.length = 0;
			const guessingPhase = JSON.parse(JSON.stringify(room.phase));
			clock.tick(10 * 1000 - 1);

			assert.deepEqual(messages, []);
			assert.deepEqual(room.phase, guessingPhase);

			clock.tick(1);

			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'reviewing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {
							mark: {author: 'mark', equivalences: [], text: 'Because'},
							matt: {author: 'matt', equivalences: [], text: 'I forget'},
							karen: {author: 'karen', equivalences: [], text: 'Ask me tomorrow'},
						},
						guesses: {
							mark: {
								matt: {actualAuthor: 'matt', suspectedAuthor: 'karen'},
								karen: {actualAuthor: 'karen', suspectedAuthor: 'matt'},
							},
							matt: {
								karen: {actualAuthor: 'karen', suspectedAuthor: 'karen'},
							},
							karen: {
								mark: {actualAuthor: 'mark', suspectedAuthor: 'mark'},
								matt: {actualAuthor: 'matt', suspectedAuthor: 'matt'},
							}
						},
					},
				}]
			);

			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {
							matt: [0, 1, 1],
							karen: [0, 1, 1],
						},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {
							karen: [1, 0, 0],
						},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {
							mark: [1, 0, 0],
							matt: [1, 0, 0],
						},
					}
				]]
			);
		});

		test('transitions to "reviewing" phase - some answers and some guesses with equivalencies', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'Because', cxnB: 'I forget', cxnC: 'Ask me tomorrow'},
			);
			room.guess('cxnC', 'mark', 'matt');
			room.guess('cxnC', 'matt', 'mark');
			room.guess('cxnB', 'karen', 'mark');
			room.guess('cxnB', 'mark', 'karen');

			// Mark and Matt are verified as equivalent. Karen and Mark are
			// not.
			room.match('cxnA', 'matt');
			room.match('cxnB', 'mark');
			room.match('cxnC', 'mark');

			room.advance('cxnA');
			room.advance('cxnB');
			messages.length = 0;
			const guessingPhase = JSON.parse(JSON.stringify(room.phase));
			clock.tick(10 * 1000 - 1);

			assert.deepEqual(messages, []);
			assert.deepEqual(room.phase, guessingPhase);

			clock.tick(1);

			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'reviewing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {
							mark: {
								author: 'mark',
								equivalences: [{author: 'matt', verified: true}],
								text: 'Because'
							},
							matt: {
								author: 'matt',
								equivalences: [{author: 'mark', verified: true}],
								text: 'I forget'
							},
							karen: {
								author: 'karen',
								equivalences: [{author: 'mark', verified: false}],
								text: 'Ask me tomorrow'
							},
						},
						guesses: {
							matt: {
								karen: {actualAuthor: 'karen', suspectedAuthor: 'mark'},
								mark: {actualAuthor: 'mark', suspectedAuthor: 'karen'},
							},
							karen: {
								mark: {actualAuthor: 'mark', suspectedAuthor: 'matt'},
								matt: {actualAuthor: 'matt', suspectedAuthor: 'mark'},
							}
						},
					},
				}]
			);

			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {
							mark: [0, 1, 1],
							karen: [0, 1, 1],
						},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {
							mark: [1, 0, 0],
							matt: [1, 0, 0],
						},
					}
				]]
			);
		});

		test('immediately transitions to "reviewing" phase when all players agree', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{},
			);
			room.advance('cxnB');
			room.advance('cxnA');
			room.advance('cxnC');
			messages.length = 0;
			clock.tick(1);

			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'reviewing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {},
						guesses: {},
					},
				}]
			);

			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				]]
			);
		});
	});

	suite('reviewing phase', () => {
		test('transitions to "asking" phase when there are zero available questions', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{},
			);
			room.advance('cxnA');
			room.advance('cxnB');
			clock.tick(10 * 1000);
			room.advance('cxnB');
			room.advance('cxnC');
			messages.length = 0;
			const reviewingPhase = JSON.parse(JSON.stringify(room.phase));
			clock.tick(10 * 1000 - 1);

			assert.deepEqual(messages, []);
			assert.deepEqual(room.phase, reviewingPhase);

			clock.tick(1);

			assert.deepEqual(
				messages,
				[{name: 'phase', data: {name: 'asking', round: 1, data: null}}]
			);
		});

		test('transitions to "answering" phase with oldest question', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{},
			);
			room.ask('cxnC', 'Why?');
			clock.tick(1);
			room.ask('cxnA', 'When?');
			room.advance('cxnA');
			room.advance('cxnB');
			clock.tick(10 * 1000);
			room.advance('cxnB');
			room.advance('cxnC');
			messages.length = 0;
			const reviewingPhase = JSON.parse(JSON.stringify(room.phase));
			clock.tick(10 * 1000 - 1);

			assert.deepEqual(messages, []);
			assert.deepEqual(room.phase, reviewingPhase);

			clock.tick(1);

			assert.equal(messages.length, 2);
			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'answering',
					round: 1,
					data: {
						endTime: Date.now() + ANSWERING_DURATION,
						question: {
							author: 'karen',
							text: 'Why?',
							timeStamp: 60000,
						},
						answers: {},
					},
				}]
			);
			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: {
							text: 'When?',
							timeStamp: 60001,
						},
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				]]
			);
		});

		test('immediately transitions to "answering" phase with oldest question when all players agree', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{},
			);
			room.ask('cxnC', 'Why?');
			clock.tick(1);
			room.ask('cxnA', 'When?');
			room.advance('cxnA');
			room.advance('cxnB');
			clock.tick(10 * 1000);
			room.advance('cxnB');
			room.advance('cxnC');
			room.advance('cxnA');
			messages.length = 0;
			clock.tick(1);

			assert.equal(messages.length, 2);
			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'answering',
					round: 1,
					data: {
						endTime: Date.now() + ANSWERING_DURATION,
						question: {
							author: 'karen',
							text: 'Why?',
							timeStamp: 60000,
						},
						answers: {},
					},
				}]
			);
			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: {
							text: 'When?',
							timeStamp: 60001,
						},
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				]]
			);
		});
	});

	suite('#advance', () => {
		suite('joining phase', () => {
			test('alerts all players', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('mike', 'cxnC');
				messages.length = 0;

				assert.equal(room.advance('cxnA'), null);

				assert.deepEqual(messages, [{
					name: 'phase',
					data: {
						name: 'joining',
						round: 0,
						data: {
							done: ['mark'],
						},
					},
				}]);
			});

			test('updates the room state', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('mike', 'cxnC');
				messages.length = 0;

				assert.equal(room.advance('cxnA'), null);

				assert.deepEqual(room.phase, {
					name: 'joining',
					round: 0,
					data: {
						done: ['mark'],
					},
				});
			});

			test('does not alert when player has already advanced', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('mike', 'cxnC');
				assert.equal(room.advance('cxnA'), null);
				messages.length = 0;

				assert.deepEqual(room.advance('cxnA'), {id: 'ERR_PLAYER_ALREADY_ADVANCED'});

				assert.deepEqual(messages, []);
			});

			test('does not transition when lone player advances', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('mike', 'cxnC');
				assert.equal(room.advance('cxnA'), null);

				assert.deepEqual(room.phase, {
					name: 'joining',
					round: 0,
					data: {
						done: ['mark'],
					},
				});
			});

			test('does not transition with unanimous two-player room', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnB'), null);

				assert.deepEqual(room.phase, {
					name: 'joining',
					round: 0,
					data: {
						done: ['mark', 'matt'],
					},
				});
			});

			test('transitions with unanimous three-player room - asking', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('karen', 'cxnC');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnB'), null);
				assert.equal(room.advance('cxnC'), null);

				assert.deepEqual(room.phase, {
					name: 'asking',
					round: 0,
					data: null,
				});
			});

			test('transitions with unanimous three-player room - answering', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('karen', 'cxnC');
				room.ask('cxnB', 'Toys, Ronny?');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnB'), null);
				assert.equal(room.advance('cxnC'), null);

				assert.deepEqual(room.phase, {
					name: 'answering',
					round: 0,
					data: {
						endTime: Date.now() + 60000,
						question: {
							author: 'matt',
							text: 'Toys, Ronny?',
							timeStamp: 0,
						},
						answers: {},
					},
				});
			});

			test('does not transition with 3 of 4 votes', () => {
				room.join('mark', 'cxnA');
				room.join('matt', 'cxnB');
				room.join('karen', 'cxnC');
				room.join('mike', 'cxnD');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnB'), null);
				assert.equal(room.advance('cxnC'), null);

				assert.deepEqual(room.phase, {
					name: 'joining',
					round: 0,
					data: {
						done: ['mark', 'matt', 'karen'],
					},
				});
			});

		});

		test('asking phase - no effect', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});

			assert.deepEqual(
				room.advance('cxnA'),
				{id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'asking'}
			);
			assert.deepEqual(
				room.advance('cxnB'),
				{id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'asking'}
			);
			assert.deepEqual(
				room.advance('cxnC'),
				{id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'asking'}
			);
			assert.deepEqual(messages, []);
		});

		test('answering phase - no effect', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnB', 'When?');
			messages.length = 0;

			assert.deepEqual(room.advance('cxnA'), {id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'answering'});
			assert.deepEqual(room.advance('cxnB'), {id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'answering'});
			assert.deepEqual(room.advance('cxnC'), {id: 'ERR_ADVANCE_OUT_OF_PHASE', context: 'answering'});
			assert.deepEqual(messages, []);
		});

		suite('guessing phase', () => {
			/** @type GuessingPhase */
			let guessingPhase;
			setup(() => {
				guessingPhase = {
					name: 'guessing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {},
						guesses: {},
					}
				};
				goToGuessingPhase(
					{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
					{author: 'cxnB', text: 'How come?'},
					{},
				);
			});

			test('alerts all players', () => {
				assert.equal(room.advance('cxnA'), null);
				guessingPhase.data.done.push('mark');

				assert.deepEqual(messages, [{
					name: 'phase',
					data: guessingPhase,
				}]);
			});

			test('updates the room state', () => {
				assert.equal(room.advance('cxnA'), null);
				guessingPhase.data.done.push('mark');

				assert.deepEqual(room.phase, guessingPhase);
			});

			test('does not alert when player has already advanced', () => {
				assert.equal(room.advance('cxnA'), null);
				messages.length = 0;
				assert.deepEqual(room.advance('cxnA'), {id: 'ERR_PLAYER_ALREADY_ADVANCED'});

				assert.deepEqual(messages, []);
			});

			test('does not update room state when player has already advanced', () => {
				assert.equal(room.advance('cxnA'), null);
				assert.deepEqual(room.advance('cxnA'), {id: 'ERR_PLAYER_ALREADY_ADVANCED'});
				guessingPhase.data.done.push('mark');

				assert.deepEqual(room.phase, guessingPhase);
			});

			test('begins countdown when majority of present players have responded', () => {
				room.join('katie', 'cxnD');
				room.leave('cxnD');
				room.join('mike', 'cxnE');
				room.leave('cxnE');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnC'), null);
				guessingPhase.data.done.push('mark');
				guessingPhase.data.done.push('karen');

				guessingPhase.data.endTime = Date.now() + 10 * 1000;
				assert.deepEqual(room.phase, guessingPhase);
			});

			test('does not restart countdown after majority of present players have responded', () => {
				room.join('katie', 'cxnD');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnC'), null);
				guessingPhase.data.done.push('mark');
				guessingPhase.data.done.push('karen');
				guessingPhase.data.endTime = Date.now() + 10 * 1000;
				clock.tick(1000);
				assert.equal(room.advance('cxnB'), null);
				guessingPhase.data.done.push('matt');

				assert.deepEqual(room.phase, guessingPhase);
			});

			test('does not begin countdown when majority of present players have not responded', () => {
				room.join('katie', 'cxnD');
				room.join('mike', 'cxnE');
				assert.equal(room.advance('cxnA'), null);
				guessingPhase.data.done.push('mark');
				room.leave('cxnA');
				assert.equal(room.advance('cxnC'), null);
				guessingPhase.data.done.push('karen');
				room.leave('cxnC');
				assert.equal(room.advance('cxnD'), null);
				guessingPhase.data.done.push('katie');

				assert.deepEqual(room.phase, guessingPhase);
			});
		});

		suite('reviewing phase', () => {
			/** @type ReviewingPhase */
			let reviewingPhase;
			setup(() => {
				reviewingPhase = {
					name: 'reviewing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'karen',
							text: 'What if I never gave you keys to the kingdom fine?',
							timeStamp: 0,
						},
						done: [],
						answers: {},
						guesses: {},
					}
				};
				goToGuessingPhase(
					{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
					{author: 'cxnC', text: 'What if I never gave you keys to the kingdom fine?'},
					{},
				);
				room.advance('cxnA');
				room.advance('cxnB');
				clock.tick(10 * 1000);
				messages.length = 0;
			});

			test('alerts all players', () => {
				assert.equal(room.advance('cxnA'), null);
				reviewingPhase.data.done.push('mark');

				assert.deepEqual(messages, [{
					name: 'phase',
					data: reviewingPhase,
				}]);
			});

			test('updates the room state', () => {
				assert.equal(room.advance('cxnA'), null);
				reviewingPhase.data.done.push('mark');

				assert.deepEqual(room.phase, reviewingPhase);
			});

			test('does not alert when player has already advanced', () => {
				assert.equal(room.advance('cxnA'), null);
				messages.length = 0;
				assert.deepEqual(room.advance('cxnA'), {id: 'ERR_PLAYER_ALREADY_ADVANCED'});

				assert.deepEqual(messages, []);
			});

			test('does not update room state when player has already advanced', () => {
				assert.equal(room.advance('cxnA'), null);
				assert.deepEqual(room.advance('cxnA'), {id: 'ERR_PLAYER_ALREADY_ADVANCED'});
				reviewingPhase.data.done.push('mark');

				assert.deepEqual(room.phase, reviewingPhase);
			});

			test('begins countdown when majority of present players have responded', () => {
				room.join('katie', 'cxnD');
				room.leave('cxnD');
				room.join('mike', 'cxnE');
				room.leave('cxnE');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnC'), null);
				reviewingPhase.data.done.push('mark');
				reviewingPhase.data.done.push('karen');

				reviewingPhase.data.endTime = Date.now() + 10 * 1000;
				assert.deepEqual(room.phase, reviewingPhase);
			});

			test('does not restart countdown after majority of present players have responded', () => {
				room.join('katie', 'cxnD');
				assert.equal(room.advance('cxnA'), null);
				assert.equal(room.advance('cxnC'), null);
				reviewingPhase.data.done.push('mark');
				reviewingPhase.data.done.push('karen');
				reviewingPhase.data.endTime = Date.now() + 10 * 1000;
				clock.tick(1000);
				assert.equal(room.advance('cxnB'), null);
				reviewingPhase.data.done.push('matt');

				assert.deepEqual(room.phase, reviewingPhase);
			});

			test('does not begin countdown when majority of present players have not responded', () => {
				room.join('katie', 'cxnD');
				room.join('mike', 'cxnE');
				assert.equal(room.advance('cxnA'), null);
				reviewingPhase.data.done.push('mark');
				room.leave('cxnA');
				assert.equal(room.advance('cxnC'), null);
				reviewingPhase.data.done.push('karen');
				room.leave('cxnC');
				assert.equal(room.advance('cxnD'), null);
				reviewingPhase.data.done.push('katie');

				assert.deepEqual(room.phase, reviewingPhase);
			});
		});
	});

	suite('#answer', () => {
		test('recognized connection ID during "joining" phase', () => {
			room.join('mark', 'cxnA');

			assert.deepEqual(
				room.answer('cxnA', 'something'),
				{id: 'ERR_ANSWER_OUT_OF_PHASE', context: 'joining'}
			);
		});

		test('recognized connection ID during "asking" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});

			assert.deepEqual(
				room.answer('cxnA', 'something'),
				{id: 'ERR_ANSWER_OUT_OF_PHASE', context: 'asking'}
			);
		});

		test('unrecognized connection ID during "answering" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnA', 'Why?');

			assert.deepEqual(
				room.answer('cxnD', 'Because'),
				{id: 'ERR_UNRECOGNIZED_CXN'}
			);
		});

		test('recognized connection ID during "answering" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnA', 'Why?');

			assert.equal(room.answer('cxnA', 'Because'), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{'mark': {author: 'mark', equivalences: [], text: 'Because'}}
			);
		});

		test('recognized connection ID during "answering" phase - retracting', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnA', 'Why?');

			room.answer('cxnA', 'Because');
			room.answer('cxnB', 'Not sure');

			assert.equal(room.answer('cxnA', null), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{'matt': {author: 'matt', equivalences: [], text: 'Not sure'}}
			);
		});

		test('advances phase when all other active players have answered', () => {
			const startTime = new Date().getTime();
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen', cxnD: 'katie'});
			room.ask('cxnA', 'Why?');
			messages.length = 0;

			room.answer('cxnA', 'Because');
			room.answer('cxnB', 'Not sure');
			room.leave('cxnD');

			assert.deepEqual(messageDataByName(messages, 'phase'), []);

			room.answer('cxnC', 'Bananas');

			clock.tick(0);

			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'guessing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'mark',
							text: 'Why?',
							timeStamp: startTime,
						},
						done: [],
						answers: {
							mark: {
								author: 'mark',
								equivalences: [],
								text: 'Because',
							},
							matt: {
								author: 'matt',
								equivalences: [],
								text: 'Not sure',
							},
							karen: {
								author: 'karen',
								equivalences: [],
								text: 'Bananas',
							},
						},
						guesses: {},
					}
				}]
			);

			messages.length = 0;

			clock.tick(ANSWERING_DURATION);

			// In a prior version of `Room`, the expediated phase transition
			// modeled by this test did not cancel the default countdown-based
			// transition. This caused the "guessing" phase to be entered
			// twice, and if any players had submitted guesses before the
			// second transition, their guesses would be discarded.
			//
			// This assertion is a regression test for that behavior.
			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[],
				'cancels previously-scheduled phase transition'
			);
		});
	});

	suite('#ask', () => {
		test('unrecognized connection ID', () => {
			room.join('mark', 'cxnA');

			assert.deepEqual(
				room.ask('cxnB', 'something'),
				{id: 'ERR_UNRECOGNIZED_CXN'}
			);

			assert.equal(
				/**@type {Player}*/(room.players.get('mark')).question, null
			);
		});

		test('recognized connection ID', () => {
			room.join('mark', 'cxnA');

			assert.equal(room.ask('cxnA', 'something'), null);

			assert.deepEqual(
				/**@type {Player}*/(room.players.get('mark')).question,
				{text: 'something', timeStamp: Date.now()}
			);
		});

		test('transitions out of "asking" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnB', 'When?');

			assert.equal(messages.length, 2);
			assert.deepEqual(
				messageDataByName(messages, 'phase'),
				[{
					name: 'answering',
					round: 0,
					data: {
						endTime: Date.now() + 60000,
						question: {
							author: 'matt',
							text: 'When?',
							timeStamp: Date.now(),
						},
						answers: {},
					}
				}]
			);
			assert.deepEqual(
				messageDataByName(messages, 'players'),
				[[
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				]]
			);
		});
	});

	suite('#guess', () => {
		test('recognized connection ID during "joining" phase', () => {
			room.join('mark', 'cxnA');

			assert.deepEqual(
				room.guess('cxnA', 'matt', 'karen'),
				{id: 'ERR_GUESS_OUT_OF_PHASE', context: 'joining'}
			);
		});

		test('recognized connection ID during "asking" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});

			assert.deepEqual(
				room.guess('cxnA', 'matt', 'karen'),
				{id: 'ERR_GUESS_OUT_OF_PHASE', context: 'asking'}
			);
		});

		suite('during "guessing" phase', () => {
			setup(() => {
				room.join('mark', 'cxnA');
				room.advance('cxnA');
				room.join('matt', 'cxnB');
				room.advance('cxnB');
				room.join('karen', 'cxnC');
				room.advance('cxnC');

				room.ask('cxnA', 'Why?');

				assert.equal(room.answer('cxnA', 'Because'), null);
				assert.equal(room.answer('cxnB', 'Not sure'), null);
				assert.equal(room.answer('cxnC', 'Don\'t care'), null);
				clock.tick(ANSWERING_DURATION);
			});

			test('unrecognized connection ID', () => {
				assert.deepEqual(
					room.guess('cxnD', 'matt', 'karen'),
					{id: 'ERR_UNRECOGNIZED_CXN'}
				);

				assert.deepEqual(
					room.state().phase,
					{
						name: 'guessing',
						round: 0,
						data: {
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: 0,
							},
							done: [],
							endTime: null,
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								karen: {
									author: 'karen',
									equivalences: [],
									text: 'Don\'t care',
								},
							},
							guesses: {},
						}
					},
				);
			});

			test('recognized connection ID - setting guess', () => {
				assert.equal(room.guess('cxnA', 'matt', 'karen'), null);

				assert.deepEqual(
					room.state().phase,
					{
						name: 'guessing',
						round: 0,
						data: {
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: 0,
							},
							done: [],
							endTime: null,
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								karen: {
									author: 'karen',
									equivalences: [],
									text: 'Don\'t care',
								},
							},
							guesses: {
								mark: {
									matt: {
										actualAuthor: 'matt',
										suspectedAuthor: 'karen',
									},
								}
							},
						}
					},
				);
			});

			test('recognized connection ID - resetting guess', () => {
				assert.equal(room.guess('cxnB', 'mark', 'karen'), null);
				assert.equal(room.guess('cxnB', 'mark', 'mark'), null);

				assert.deepEqual(
					room.state().phase,
					{
						name: 'guessing',
						round: 0,
						data: {
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: 0,
							},
							done: [],
							endTime: null,
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								karen: {
									author: 'karen',
									equivalences: [],
									text: 'Don\'t care',
								},
							},
							guesses: {
								matt: {
									mark: {
										actualAuthor: 'mark',
										suspectedAuthor: 'mark',
									},
								}
							},
						}
					},
				);
			});

			test('recognized connection ID - unsetting guess', () => {
				assert.equal(room.guess('cxnB', 'mark', 'karen'), null);
				assert.equal(room.guess('cxnB', 'karen', 'mark'), null);
				assert.equal(room.guess('cxnB', 'mark', null), null);

				assert.deepEqual(
					room.state().phase,
					{
						name: 'guessing',
						round: 0,
						data: {
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: 0,
							},
							done: [],
							endTime: null,
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								karen: {
									author: 'karen',
									equivalences: [],
									text: 'Don\'t care',
								},
							},
							guesses: {
								matt: {
									karen: {
										actualAuthor: 'karen',
										suspectedAuthor: 'mark',
									},
								}
							},
						}
					},
				);
			});

			test('recognized connection ID - unsetting only guess', () => {
				assert.equal(room.guess('cxnB', 'mark', 'karen'), null);
				assert.equal(room.guess('cxnB', 'mark', null), null);

				assert.deepEqual(
					room.state().phase,
					{
						name: 'guessing',
						round: 0,
						data: {
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: 0,
							},
							done: [],
							endTime: null,
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								karen: {
									author: 'karen',
									equivalences: [],
									text: 'Don\'t care',
								},
							},
							guesses: {},
						}
					},
				);
			});
		});
	});

	suite('#join', () => {
		test('updates players -- first player', () => {
			room.join('mark', 'cxnA');

			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					}
				],
			}]);
		});

		test('updates players -- second player', () => {
			room.join('mark', 'cxnA');
			messages.length = 0;

			room.join('matt', 'cxnB');
			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnB',
						standings: {},
					}
				],
			}]);
		});

		test('ignores attempts to join with the same name as an active player', () => {
			room.join('mark', 'cxnA');
			messages.length = 0;

			room.join('mark', 'cxnB');
			assert.deepEqual(messages, []);
			assert.equal(
				/**@type {Player}*/(room.players.get('mark')).cxnId, 'cxnA'
			);
		});

		test('permits attempts to join with the same name as an inactive player (returning player with a non-zero score)', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnC', text: 'What if I never gave you keys to the kingdom fine?'},
				{},
			);
			room.guess('cxnB', 'mark', 'karen');
			room.advance('cxnA');
			room.advance('cxnB');
			clock.tick(10 * 1000);
			messages.length = 0;

			room.leave('cxnB');

			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: null,
						standings: {
							mark:  [0, 0, 1],
							karen: [0, 1, 0],
						},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				],
			}]);

			messages.length = 0;

			room.join('matt', 'cxnD');

			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnD',
						standings: {
							mark:  [0, 0, 1],
							karen: [0, 1, 0],
						},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				],
			}]);
		});

		test('does not transition out of "joining" phase and into the "asking" phase', () => {
			room.join('mark', 'cxnA');
			room.join('matt', 'cxnB');
			messages.length = 0;

			room.join('karen', 'cxnC');
			assert.deepEqual(
				messages,
				[{
					name: 'players',
					data: [
						{
							avatar: null,
							name: 'mark',
							question: null,
							cxnId: 'cxnA',
							standings: {},
						},
						{
							avatar: null,
							name: 'matt',
							question: null,
							cxnId: 'cxnB',
							standings: {},
						},
						{
							avatar: null,
							name: 'karen',
							question: null,
							cxnId: 'cxnC',
							standings: {},
						}
					],
				}]
			);
		});

		test('does not transition out of "joining" phase and into the "answering" phase', () => {
			room.join('mark', 'cxnA');
			room.join('matt', 'cxnB');
			room.ask('cxnB', 'How?');
			messages.length = 0;

			room.join('karen', 'cxnC');
			assert.deepEqual(
				messages,
				[{
					name: 'players',
					data: [
						{
							avatar: null,
							name: 'mark',
							question: null,
							cxnId: 'cxnA',
							standings: {},
						},
						{
							avatar: null,
							name: 'matt',
							question: {
								text: 'How?',
								timeStamp: Date.now(),
							},
							cxnId: 'cxnB',
							standings: {},
						},
						{
							avatar: null,
							name: 'karen',
							question: null,
							cxnId: 'cxnC',
							standings: {},
						}
					],
				}]
			);
		});
	});

	suite('#leave', () => {
		test('some players remaining', () => {
			room.join('mark', 'cxnB');
			room.join('matt', 'cxnC');
			messages.length = 0;

			room.leave('cxnB');

			assert.equal(ends.length, 0);
			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					}
				],
			}]);
		});

		test('no players remaining', () => {
			room.join('mark', 'cxnB');
			room.join('matt', 'cxnC');
			room.leave('cxnB');
			messages.length = 0;
			ends.length = 0;

			room.leave('cxnC');

			assert.equal(ends.length, 1);
			assert.deepEqual(messages, []);
		});

		test('persists players with some standings', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because'},
			);
			room.guess('cxnB', 'mark', 'karen');
			room.advance('cxnA');
			room.advance('cxnB');
			clock.tick(10 * 1000);

			messages.length = 0;
			ends.length = 0;

			room.leave('cxnB');

			assert.equal(ends.length, 0);
			assert.deepEqual(messages, [{
				name: 'players',
				data: [
					{
						avatar: null,
						name: 'mark',
						question: null,
						cxnId: 'cxnA',
						standings: {},
					},
					{
						avatar: null,
						name: 'matt',
						question: null,
						cxnId: null,
						standings: {
							mark: [0, 0, 1],
							karen: [0, 1, 0],
						},
					},
					{
						avatar: null,
						name: 'karen',
						question: null,
						cxnId: 'cxnC',
						standings: {},
					},
				],
			}]);
		});

		suite('answering phase', () => {
			test('advances phase when all other active players have answered', () => {
				const startTime = new Date().getTime();
				goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen', cxnD: 'katie', cxnE: 'john'});
				room.ask('cxnA', 'Why?');
				messages.length = 0;

				room.answer('cxnA', 'Because');
				room.answer('cxnB', 'Not sure');
				room.answer('cxnD', 'Bananas');
				room.leave('cxnE');

				assert.deepEqual(messageDataByName(messages, 'phase'), []);

				room.leave('cxnC');

				clock.tick(0);

				assert.deepEqual(
					messageDataByName(messages, 'phase'),
					[{
						name: 'guessing',
						round: 0,
						data: {
							endTime: null,
							question: {
								author: 'mark',
								text: 'Why?',
								timeStamp: startTime,
							},
							done: [],
							answers: {
								mark: {
									author: 'mark',
									equivalences: [],
									text: 'Because',
								},
								matt: {
									author: 'matt',
									equivalences: [],
									text: 'Not sure',
								},
								katie: {
									author: 'katie',
									equivalences: [],
									text: 'Bananas',
								},
							},
							guesses: {},
						}
					}]
				);

				messages.length = 0;

				clock.tick(ANSWERING_DURATION);

				// In a prior version of `Room`, the expediated phase
				// transition modeled by this test did not cancel the default
				// countdown-based transition. This caused the "guessing" phase
				// to be entered twice, and if any players had submitted
				// guesses before the second transition, their guesses would be
				// discarded.
				//
				// This assertion is a regression test for that behavior.
				assert.deepEqual(
					messageDataByName(messages, 'phase'),
					[],
					'cancels previously-scheduled phase transition'
				);
			});
		});

		suite('guessing phase', () => {
			/** @type GuessingPhase */
			let guessingPhase;
			setup(() => {
				guessingPhase = {
					name: 'guessing',
					round: 0,
					data: {
						endTime: null,
						question: {
							author: 'matt',
							text: 'How come?',
							timeStamp: 0,
						},
						done: [],
						answers: {},
						guesses: {},
					}
				};
				goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
				room.ask('cxnB', 'How come?');
				clock.tick(ANSWERING_DURATION);
				messages.length = 0;
			});

			test('does not begin countdown when departure does not trigger majority', () => {
				room.join('katie', 'cxnD');
				room.join('mike', 'cxnE');
				room.join('john', 'cxnF');
				room.advance('cxnE');
				guessingPhase.data.done.push('mike');
				room.advance('cxnF');
				guessingPhase.data.done.push('john');
				messages.length = 0;

				room.leave('cxnA');

				assert.deepEqual(
					messageDataByName(messages, 'phase'),
					[],
				);
				assert.deepEqual(room.phase, guessingPhase);
			});

			test('begins countdown when departure triggers majority', () => {
				room.join('katie', 'cxnD');
				room.join('mike', 'cxnE');
				room.join('john', 'cxnF');
				room.advance('cxnE');
				guessingPhase.data.done.push('mike');
				room.advance('cxnF');
				guessingPhase.data.done.push('john');
				room.leave('cxnA');
				messages.length = 0;
				room.leave('cxnB');
				guessingPhase.data.endTime = Date.now() + 10000;

				assert.deepEqual(
					messageDataByName(messages, 'phase'),
					[guessingPhase],
				);
				assert.deepEqual(room.phase, guessingPhase);
			});
		});
	});

	suite('#match', () => {
		test('recognized connection ID during "joining" phase', () => {
			room.join('mark', 'cxnA');

			assert.deepEqual(
				room.match('cxnA', 'matt'),
				{id: 'ERR_MATCH_OUT_OF_PHASE', context: 'joining'}
			);
		});

		test('recognized connection ID during "asking" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});

			assert.deepEqual(
				room.match('cxnA', 'matt'),
				{id: 'ERR_MATCH_OUT_OF_PHASE', context: 'asking'}
			);
		});

		test('recognized connection ID during "answering" phase', () => {
			goToAskingPhase({cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'});
			room.ask('cxnA', 'Why?');

			assert.deepEqual(
				room.match('cxnA', 'matt'),
				{id: 'ERR_MATCH_OUT_OF_PHASE', context: 'answering'}
			);
		});

		test('unrecognized connection ID during "guessing" phase', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because'},
			);

			assert.deepEqual(
				room.match('cxnD', 'matt'),
				{id: 'ERR_UNRECOGNIZED_CXN'}
			);
		});

		test('recognized connection ID during "guessing" phase - non-existent guess', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because'},
			);

			assert.deepEqual(
				room.match('cxnB', 'mark'),
				{id: 'ERR_ANSWER_NOT_FOUND', context: 'matt'}
			);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{'mark': {author: 'mark', equivalences: [], text: 'because'}}
			);
		});

		test('recognized connection ID during "guessing" phase - non-existent match', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because'},
			);

			assert.deepEqual(
				room.match('cxnA', 'stranger'),
				{id: 'ERR_ANSWER_NOT_FOUND', context: 'stranger'}
			);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{'mark': {author: 'mark', equivalences: [], text: 'because'}}
			);

			assert.deepEqual(
				room.match('cxnA', 'matt'),
				{id: 'ERR_ANSWER_NOT_FOUND', context: 'matt'}
			);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{'mark': {author: 'mark', equivalences: [], text: 'because'}}
			);
		});

		test('recognized connection ID during "guessing" phase - existent match', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because', cxnB: 'not sure'},
			);

			// Silently tolerates matching yourself
			assert.deepEqual(room.match('cxnA', 'mark'), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{
					'mark': {author: 'mark', equivalences: [], text: 'because'},
					'matt': {author: 'matt', equivalences: [], text: 'not sure'},
				}
			);

			// Correctly accepts valid match
			assert.deepEqual(room.match('cxnA', 'matt'), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{
					'mark': {author: 'mark', equivalences: [{author: 'matt', verified: false}], text: 'because'},
					'matt': {author: 'matt', equivalences: [], text: 'not sure'},
				}
			);
			assert.deepEqual(messageDataByName(messages, 'phase'), []);

			// Silently tolerates duplicated match
			assert.deepEqual(room.match('cxnA', 'matt'), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{
					'mark': {author: 'mark', equivalences: [{author: 'matt', verified: false}], text: 'because'},
					'matt': {author: 'matt', equivalences: [], text: 'not sure'},
				}
			);
			assert.deepEqual(messageDataByName(messages, 'phase'), []);
		});

		test('recognized connection ID during "guessing" phase - verification', () => {
			goToGuessingPhase(
				{cxnA: 'mark', cxnB: 'matt', cxnC: 'karen'},
				{author: 'cxnB', text: 'How come?'},
				{cxnA: 'because', cxnB: 'not sure'},
			);
			assert.deepEqual(room.match('cxnA', 'matt'), null);

			assert.deepEqual(messageDataByName(messages, 'phase'), []);

			assert.deepEqual(room.match('cxnB', 'mark'), null);

			assert.deepEqual(
				/**@type {AnsweringPhase} */(room.phase).data.answers,
				{
					'mark': {author: 'mark', equivalences: [{author: 'matt', verified: true}], text: 'because'},
					'matt': {author: 'matt', equivalences: [{author: 'mark', verified: true}], text: 'not sure'},
				}
			);
			assert.deepEqual(messageDataByName(messages, 'phase'), [room.phase]);
		});
	});
});
