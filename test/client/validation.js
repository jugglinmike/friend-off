import * as assert from 'assert';
import {validateRoomName} from '../../src/client/validation.js';

suite('validation', () => {
	suite('validateRoomName', () => {
		test('valid', () => {
			assert.deepEqual(
				validateRoomName('foo123'),
				{isValid: true, value: 'foo123'}
			);
		});

		test('valid, replaced spaces', () => {
			assert.deepEqual(
				validateRoomName('foo bar baz'),
				{isValid: true, value: 'foo-bar-baz'}
			);
		});

		test('valid, lowercased', () => {
			assert.deepEqual(
				validateRoomName('McLaughlin'),
				{isValid: true, value: 'mclaughlin'}
			);
		});

		test('too long', () => {
			assert.deepEqual(
				validateRoomName('foobarbazfoobarbazfoo'),
				{isValid: false, reason: 'Room names may not exceed 20 characters in length'}
			);
		});

		test('bad character', () => {
			assert.deepEqual(
				validateRoomName('foo?bar'),
				{isValid: false, reason: 'Room names may only contain letters, numbers, and dashes'}
			);
		});
	});
});
