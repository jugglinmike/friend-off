import * as assert from 'assert';
import getAlternates from '../../../src/client/util/get-alternates.js';

/** @typedef {import("../../../src/types").AllAnswers} AllAnswers */

suite('getAlternates', () => {
	/** @type {AllAnswers} */
	let answers;

	setup(() => {
		answers = {
			mike: {
				author: 'mike',
				equivalences: [],
				text: 'Captain Crunch'
			},
			mark: {
				author: 'mark',
				equivalences: [],
				text: 'Frosted Flakes'
			},
			matt: {
				author: 'matt',
				equivalences: [],
				text: 'Count Chocula'
			},
			karen: {
				author: 'karen',
				equivalences: [],
				text: 'Lucky Charms'
			},
		};
	});

	test('no alternates', () => {
		assert.deepEqual(getAlternates(answers.mike, answers), []);
		assert.deepEqual(getAlternates(answers.mark, answers), []);
		assert.deepEqual(getAlternates(answers.matt, answers), []);
		assert.deepEqual(getAlternates(answers.karen, answers), []);
	});

	test('no verified alternates', () => {
		answers.mike.equivalences = [
			{author: 'mark', verified: false},
			{author: 'matt', verified: false},
		];
		answers.mike.equivalences = [
			{author: 'karen', verified: false},
		];

		assert.deepEqual(getAlternates(answers.mike, answers), []);
		assert.deepEqual(getAlternates(answers.mark, answers), []);
		assert.deepEqual(getAlternates(answers.matt, answers), []);
		assert.deepEqual(getAlternates(answers.karen, answers), []);
	});

	test('one verified alternate', () => {
		answers.mark.equivalences = [
			{author: 'matt', verified: true},
		];
		answers.matt.equivalences = [
			{author: 'mark', verified: true},
		];

		assert.deepEqual(getAlternates(answers.mike, answers), []);
		assert.deepEqual(getAlternates(answers.mark, answers), ['Count Chocula']);
		assert.deepEqual(getAlternates(answers.matt, answers), ['Frosted Flakes']);
		assert.deepEqual(getAlternates(answers.karen, answers), []);
	});

	test('multiple verified alternates', () => {
		answers.mark.equivalences = [
			{author: 'matt', verified: true},
		];
		answers.matt.equivalences = [
			{author: 'mark', verified: true},
			{author: 'karen', verified: true},
		];
		answers.karen.equivalences = [
			{author: 'matt', verified: true},
		];

		assert.deepEqual(getAlternates(answers.mike, answers), []);
		assert.deepEqual(getAlternates(answers.mark, answers), ['Count Chocula']);
		assert.deepEqual(getAlternates(answers.matt, answers), ['Frosted Flakes', 'Lucky Charms']);
		assert.deepEqual(getAlternates(answers.karen, answers), ['Count Chocula']);
	});
});
