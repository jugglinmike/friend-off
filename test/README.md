# The `test` directory

This directory contains the code which automatically verifies that the
application is serving its intended purpose.

The sub-directory named "client" verifies the correctness of the code intended
for the "client" (i.e. the web browser), and it does this with [the Mocha
testing framework](https://mochajs.org/). The code under test is stored in the
`src/client` directory.

The sub-directory named "server" verifies the correctness of the code intended
for the server, and (like the "client" test suite) it does this with [the Mocha
testing framework](https://mochajs.org/). The code under test is stored in the
`src/server` directory.

The sub-directory named "functional" also verifies the correctness of the
server code, but it does so at a higher level of abstraction.
