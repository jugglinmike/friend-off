'use strict';
const assert = require('assert');
const fs = require('fs').promises;
const path = require('path');

const licenseChecker = require('license-checker');

const assertObject = (value) => {
	assert.equal(typeof value, 'object');
	assert.notEqual(value, null);
};

/**
 * Only packages included in the code delivered to the client should be
 * summarized here. The `license-checker` package is not capable of the
 * filtering this requires (the filtering API it provides only operates on
 * direct dependencies). Create a temporary project directory describing a
 * package with only the client-side dependencies present. Invoke the provided
 * function, and remove the temporary project directory once it is complete.
 */
const withClientPackageJson = async (whileExists) => {
	const manifest = require('../package.json');
	const alternateDir = path.join(process.cwd(), 'tmp');

	assertObject(manifest);
	assertObject(manifest.dependencies);
	// Exhaustively validate dependencies so that any future change triggers a
	// build failure and a verification of the expected behavior.
	assert.deepEqual(
		Object.keys(manifest.dependencies),
		['debug', 'express', 'htm', 'preact', 'socket.io', 'socket.io-client']
	);

	delete manifest.dependencies.debug;
	delete manifest.dependencies.express;
	delete manifest.dependencies['socket.io'];

	await fs.mkdir(alternateDir, {recursive: true});

	try {
		await fs.symlink(
			path.join(process.cwd(), 'node_modules'),
			path.join(alternateDir, 'node_modules'),
			'dir'
		);
		await fs.writeFile(
			path.join(alternateDir, 'package.json'),
			JSON.stringify(manifest, null, 2),
			'utf-8'
		);

		return await whileExists(alternateDir);
	} finally {
		await fs.rm(alternateDir, {recursive: true});
	}
};

const find = async (packageManifest) => {
	const projects = await new Promise((resolve, reject) => {
		licenseChecker.init({ start: packageManifest, production: true}, (error, data) => {
			if (error) {
				reject(error);
				return;
			}
			resolve(data);
		});
	});

	const formatted = Object.entries(projects)
			.map(([nameAndVersion, project]) => {
				const [, name, version] = nameAndVersion
					.match(/(.*)@([^@]+)$/);

				if (name === 'friendoff') {
					return null;
				}

				return {
					name,
					version,
					repository: project.repository,
					licenseName: project.licenses,
					licenseFile: project.licenseFile
				};
			})
			.filter((project) => !!project);

	return Promise.all(
		formatted.map(async (project) => {
			return {
				...project,
				license: {
					name: project.licenseName,
					text: await fs.readFile(project.licenseFile, 'utf-8')
				}
			};
		})
	);
};

module.exports = () => {
	return {
		name: 'snowpack-plugin-licenses',
		resolve: {
			input: ['.licenses'],
			output: ['.js'],
		},
		async load() {
			const data = await withClientPackageJson(find);

			return Promise.resolve(`export default ${JSON.stringify(data)};`);
		},
	};
};
