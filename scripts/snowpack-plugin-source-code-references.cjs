'use strict';

const assert = require('assert');
const fs = require('fs').promises;
const path = require('path');
const textFilenamePattern = /\.(js|css|svg|html|yaml|ts|md)$/i;
const regionPattern = /^\s*(?:\/\/|<!--) (BEGIN|END) NOTABLE SOURCE CODE REGION "(.+)"(?: -->)?$/;

class FileContentError extends Error {
	constructor(filename, lineNumber, line) {
		super(`Invalid file content in ${filename}:${lineNumber}\n> ${line}`);
	}
}

async function * walk(directory) {
	for await (const item of await fs.opendir(directory)) {
		const location = path.join(directory, item.name);

		if (item.isDirectory()) {
			yield * walk(location);
		} else if (item.isFile()) {
			yield location;
		}
	}
}

async function searchFile(filename) {
	return (await fs.readFile(filename, 'utf-8')).split('\n')
		.map((line, index) => {
			if (line.includes('NOTABLE SOURCE CODE REGION')) {
				const match = regionPattern.exec(line);
				const lineNumber = index + 1;

				if (!match) {
					throw new FileContentError(filename, lineNumber, line);
				}

				return {lineNumber, boundary: match[1], name: match[2]};
			}
		})
		.filter((item) => !!item)
		.reduce((all, {lineNumber, boundary, name}) => {
			if (boundary === 'BEGIN') {
				assert(!all.has(name));
				all.set(name, {filename, start: lineNumber + 1});
			} else {
				const item = all.get(name);
				assert(!!item);
				assert(!('end' in item));
				item.end = lineNumber - 1;
			}
			return all;
		}, new Map());
}

async function findRegions() {
	const projectRoot = path.resolve(__dirname, '..');
	const srcDir = path.join(projectRoot, 'src');
	const regions = new Map();

	for await (const filename of walk(srcDir)) {
		if (!textFilenamePattern.test(filename)) {
			continue;
		}

		for (const [key, value] of await searchFile(filename)) {
			const relativeFilename = path.relative(projectRoot, value.filename);
			regions.set(key, {...value, filename: relativeFilename});
		}
	}

	return regions;
}

function generateCode(regions, repositoryWebRoot) {
	return [...regions.entries()]
		.map(([name, {filename, start, end}]) => {
			const url = `${repositoryWebRoot}/${filename}#L${start}-${end}`;
			return `export const ${name} = ${JSON.stringify(url)};`;
		})
		.join('\n') + '\nexport default null;';
}

module.exports = () => {
	return {
		name: 'snowpack-plugin-source-code-references',
		resolve: {
			input: ['.notable-source-code-regions'],
			output: ['.js'],
		},
		async load() {
			const {repositoryWebRoot} = require('../package.json');
			const regions = await findRegions();

			return Promise.resolve(generateCode(regions, repositoryWebRoot));
		},
	};
};

if (require.main === module) {
	(async() => {
		console.log(await (module.exports()).load());
	})();
}
