#!/bin/bash

set -ex

npm run build

git branch --delete --force build || true

function cleanup {
  git checkout -
}

trap cleanup EXIT

git checkout --force --orphan build

sed -i -e '/build\//d' .gitignore

git add .gitignore build

git commit -m Build

git push --force glitch build:main
