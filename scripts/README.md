# The `scripts` directory

This directory contains the code used to prepare the source code for execution.

The files whose name begin with `snowpack-plugin-` are plugins for [the
Snowpack build tool](https://www.snowpack.dev/). They dynamically generate
JavaScript code for information which exists in other formats and would
otherwise need to be manually duplicated (e.g. the software licenses for the
project's client-side dependencies, or the line numbers of notable sections of
source code).

The `deploy.sh` file is a [GNU Bash](https://www.gnu.org/software/bash/) script
that encapsulates the steps necessary to publish the code to the production
server. Contributors typically will not need to use this script.
